//
//  MainVC.swift
//  Test
//
//  Created by paparotnick on 02.11.16.
//  Copyright © 2016 Zappa. All rights reserved.
//

import Darwin
import UIKit
import Foundation
import Accelerate
import AVFoundation

// MARK: - Constants

let dis_frequency = 44100
let buffer_size = 16384
let threshold = 100
let bus = 0
let isTest: Bool! = false

class MainVC: UIViewController {
    

    let engine = AVAudioEngine()
    var isStart: Bool! = false
    
    @IBOutlet weak var startStopButton: UIButton?
    @IBOutlet weak var frequencyLabel: UILabel?
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVC()
    }
    
    func initVC(){
        if (isTest == true){
            startStopButton?.titleLabel?.text = "Test"
        }
    }
    
    // MARK: - Test Mode
    
    func test_signal(){
        var signal:Array<Double>?
        signal = getTestSignalArray(leng: buffer_size)
        let fftData = fft(signal!)
        getMaxFromFFTarray(fftData)
    }
    
    func getTestSignalArray(leng:Int) -> Array<Double>{
        var signal = [Double]()
        print(leng)
        for t in 0...leng-1 {
            let newVal = 6.0 * sin(2.0 * Double.pi * Double(t) * 440 / Double(dis_frequency))
            signal.append(newVal)
        }
        return signal
    }
    
    // MARK: - Work with signal
    
    func start() {
        if (isStart == false){
            return
        }
        
        let input = engine.inputNode!
        
        input.installTap(onBus: bus, bufferSize: AVAudioFrameCount(buffer_size), format: input.inputFormat(forBus: bus)) { (buffer, time) -> Void in
            let samples = buffer.floatChannelData?[0]
            self.getMaxFluerFromArray(samples!)
        }
        
        try! engine.start()
    }
    
    func stop() {
        if (isStart == true){
            return
        }
        
        frequencyLabel?.text = "0"
        let input = engine.inputNode!
        input.removeTap(onBus: bus)
        try! engine.stop()
    }
    
    func getMaxFluerFromArray(_ input: UnsafeMutablePointer<Float>){
        
        let array:[Double] = arrayFromPointer(input: input, size: buffer_size)
        let fftData = fft(array)
        getMaxFromFFTarray(fftData)

    }
    
    func getMaxFromFFTarray(_ input:[Double]){
        
        let count:Int = input.count
        var frequencyWithMaxAmp = 0.0
        var maxFFTval = Double(threshold)
        var max_i = 0
        for i in 0...count/2-1
        {
            if input[i] > maxFFTval {
                maxFFTval = input[i]
                max_i = i
                frequencyWithMaxAmp = Double(i) * Double(dis_frequency) / Double(buffer_size)
            }
        }
        let doubleFormat = "%.2f"
        if (max_i == 0) || (max_i == count){
            return
        }
        let x = get_x(x1: max_i-1, x2: max_i, x3: max_i+1, y1: input[max_i-1], y2: input[max_i], y3: input[max_i+1])
        let xFrequency = Double(x) * Double(dis_frequency) / Double(buffer_size)
        DispatchQueue.main.async {
            self.frequencyLabel?.text = NSString(format:doubleFormat as NSString, xFrequency) as String
        }
    }
    
    
    func arrayFromPointer( input: UnsafeMutablePointer<Float>, size:Int) -> [Double]{
        
        let array = Array(UnsafeBufferPointer(start: input, count: size))
        
        
        var signal = [Double]()
        for item in array {
            signal.append(Double(item))
        }
        return signal
    }
    

    // MARK: - IBActions
    
    @IBAction func tabBut(sender: UIButton) {
        
        if (isTest == true){
            test_signal()
            return
        }
        
        if (isStart == false){
            isStart = true
            DispatchQueue.main.async {
                self.startStopButton?.titleLabel?.text = "Stop"
            }
            start()
        } else{
            isStart = false
            DispatchQueue.main.async {
                self.startStopButton?.titleLabel?.text = "Start"
            }
            stop()
        }
        
    }

    // MARK: - Fast Furie Transition
    
    func fft(_ input: [Double]) -> [Double] {
        var real = [Double](input)
        var imaginary = [Double](repeating: 0.0, count: input.count)
        var splitComplex = DSPDoubleSplitComplex(realp: &real, imagp: &imaginary)
        
        let length = vDSP_Length(floor(log2(Float(input.count))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetupD(length, radix)
        vDSP_fft_zipD(weights!, &splitComplex, 1, length, FFTDirection(FFT_FORWARD))
        
        var magnitudes = [Double](repeating: 0.0, count: input.count)
        vDSP_zvmagsD(&splitComplex, 1, &magnitudes, 1, vDSP_Length(input.count))
        return magnitudes
    }
    
    // MARK: - Get Extremum
    
    func get_z(x1:Double, x2:Double, x3:Double, y1:Double, y2:Double, y3:Double) -> [Double] {
        return [x1*x1-x2*x2, x1-x2, y1-y2,  x2*x2-x3*x3, x2-x3, y2-y3]
    }
    
    func get_b(z11:Double,z12:Double,z13:Double,z21:Double,z22:Double,z23:Double) -> Double{
        return (z21*z13 - z23*z11) / (z12*z21 - z22*z11)
    }
    
    func get_a(z13:Double, z12:Double, b:Double, z11:Double) -> Double{
        return (z13 - b*z12) / z11
    }
    func get_x(x1:Int, x2:Int, x3:Int, y1:Double, y2:Double, y3:Double)-> Double{
        let z = get_z(x1: Double(x1),x2: Double(x2),x3: Double(x3), y1: Double(y1), y2: Double(y2), y3: Double(y3))
        let b = get_b(z11: z[0], z12: z[1], z13: z[2],z21: z[3],z22: z[4],z23: z[5])
        let a = get_a(z13: z[2], z12: z[1],   b: b,   z11: z[0])
        return -b/(2*a)
    }
}
